use std::io;

fn main() {
    let x = fibonacci();
    println!("{}", x);
}

fn input() -> i32 {
    println!("Enter a number: ");
    let mut n = String::new();

    io::stdin().read_line(&mut n).expect("Failed to read line");

    let n: i32 = n.trim().parse().expect("Please input a number");
    return n;
}

fn fibonacci() -> i32 {
    let k = input();
    let mut a = 0;
    let mut b = 1;
    let mut c = 0;
    let mut _i = 1;

    if k == a {
        return k;
    } else if k == b {
        return b;
    }

    while _i < k {
        c = a + b;
        a = b;
        b = c;
        _i += 1;
    }
    return c;
}
